package com.example.ventadeautos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

public class Login extends AppCompatActivity {

    EditText usertxt,passwordtxt;
    Button loginbtn;
    String[] arreglousuarios,arreglopasswords;
    String userstrng = "Mario:Arturo:Jesus:Africa:Adriana";
    String passwordstrng = "12345:12345:12345:12345:12345";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        arreglousuarios = userstrng.split(":");
        arreglopasswords = passwordstrng.split(":");

        usertxt = (EditText) findViewById(R.id.usertxt);
        passwordtxt = (EditText) findViewById(R.id.passtxt);
        loginbtn = (Button) findViewById(R.id.logibtn);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iniciarsesion();

            }
        });

    }

    public void iniciarsesion(){

        String user = usertxt.getText().toString();
        String pass = passwordtxt.getText().toString();

        if(Arrays.asList(arreglousuarios).contains(user))
        {
            if(Arrays.asList(arreglopasswords).contains(pass))
            {
                Intent view = new Intent(Login.this,MainActivity.class);
                startActivity(view);
                finish();
            }
            else
            {
                Toast.makeText(this, "Vuelve a intentarlo", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "Vuelve a intentarlo", Toast.LENGTH_SHORT).show();
        }

    }
}
