package com.example.ventadeautos;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class new_user extends AppCompatActivity {

   EditText nombretxt,apellidotxt,telefonotxt,emailtxt,direcciontxt;
   Button guardar;
   String nombre,apellido,telefono,email,direccion;
   UserDatabase ud;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        // ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF0000")));
        ab.setDisplayShowTitleEnabled(true);

        nombretxt = (EditText) findViewById(R.id.editnombre);
        apellidotxt = (EditText) findViewById(R.id.editape);
        telefonotxt = (EditText) findViewById(R.id.editnum);
        emailtxt =(EditText) findViewById(R.id.editcorreo);
        direcciontxt = (EditText) findViewById(R.id.editdireccion);
        guardar = (Button) findViewById(R.id.button);

        ud = UserDatabase.getAppDatabase(new_user.this);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                agregarusuario();

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void agregarusuario()
    {
        nombre = nombretxt.getText().toString();
        apellido = apellidotxt.getText().toString();
        telefono = telefonotxt.getText().toString();
        email = emailtxt.getText().toString();
        direccion = direcciontxt.getText().toString();

        new add_user(new_user.this).execute();
    }

    public class add_user extends AsyncTask<String, Void, String> {

        private Context context;

        public add_user(Context context) {
            this.context = context;
        }

        ProgressDialog pd = new ProgressDialog(new_user.this);

        protected void onPreExecute() {

            pd.setMessage("Agregando Usuario...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... arg0) {

            String result = "";

            User u = new User();
            u.setFirstName(nombre);
            u.setLastName(apellido);
            u.setPhone(telefono);
            u.setEmail(email);
            u.setAddress(direccion);

            UserDao dao = ud.userDao();
            dao.insert(u);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            pd.hide();
            pd.dismiss();

            nombretxt.setText("");
            apellidotxt.setText("");
            telefonotxt.setText("");
            emailtxt.setText("");
            direcciontxt.setText("");

            Toast.makeText(new_user.this, "Usuario Guardado", Toast.LENGTH_SHORT).show();

        }
    }


}
