package com.example.ventadeautos;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapterUsers extends BaseAdapter {


    private List<User> listData;
    Context context;



    public CustomListAdapterUsers(Context context, List<User> listData) {
        this.listData = listData;
        this.context = context;

    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.row_user, null);
            holder = new ViewHolder();

            holder.nombretxt = (TextView) convertView.findViewById(R.id.textnombre);
            holder.idtxt = (TextView) convertView.findViewById(R.id.textid);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int id = listData.get(position).getId();
        String idstring = String.valueOf(id);
        holder.nombretxt.setText(listData.get(position).getFirstName() + " " + listData.get(position).getLastName());
        holder.idtxt.setText("ID: " + idstring);

        return convertView;
    }




    static class ViewHolder {
        TextView nombretxt;
        TextView idtxt;

    }

}
