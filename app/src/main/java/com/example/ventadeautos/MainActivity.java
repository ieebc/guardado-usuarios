package com.example.ventadeautos;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    //String[] nombresautos;
    //String autos = "Camry:Mazda:Cobalt:Mustang:Charger:Fusion:Patriot:cherokee";
    //ListView listautos;
    List<User> users;
    FloatingActionButton fab_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new get_users(MainActivity.this).execute();

        fab_user = (FloatingActionButton) findViewById(R.id.fabnew);
        fab_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(MainActivity.this,new_user.class);
                startActivity(view);
            }
        });

        new get_users(MainActivity.this).execute();

    }


    @Override
    protected void onRestart() {
        super.onRestart();

        new get_users(MainActivity.this).execute();

    }

    public class get_users extends AsyncTask<String, Void, String> {

        private Context context;

        public get_users(Context context) {
            this.context = context;
        }

        ProgressDialog pd = new ProgressDialog(MainActivity.this);

        protected void onPreExecute() {

            pd.setMessage("Cargando usuarios");
            pd.show();

        }

        @Override
        protected String doInBackground(String... arg0) {

            String result = "";

            UserDatabase ud = UserDatabase.getAppDatabase(MainActivity.this);

            UserDao dao = ud.userDao();
            //obtener usuarios
            users = dao.getAllUsers();

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            final ListView Lista = (ListView) findViewById(R.id.listautos);
            Lista.setAdapter(new CustomListAdapterUsers(MainActivity.this, users));

            pd.hide();
            pd.dismiss();



        }
    }

}
